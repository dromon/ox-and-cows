exe = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(exe) unless $LOAD_PATH.include?(exe)
require "NyNewGem/version"

Gem::Specification.new do |spec|
  spec.name          = "Oxs&Cows"
  spec.version       = Oxs&Cows::VERSION
  spec.authors       = ["Maria Smirnova"]
  spec.email         = ["0.foron@gmail.com"]
  spec.summary       = %q{Game that you can play in your console!}
  spec.description   = %q{Try to outfit this magnificient piece of coding! Be brave and challenge yourself to game so light, it could run on Windows 3!
  spec.homepage      = "www.i_dont_have_any_webpages.com"
  spec.license       = "MIT"


  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do #Dir внезапно меняет папки
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "lib"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.17"
  spec.add_development_dependency "rake", "~> 10.0"
end
